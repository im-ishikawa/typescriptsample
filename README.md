# 開発環境構築 for TypeScript　

### Node.jsとnpmをインストール
https://nodejs.org/

    >node -v
    >npm -v

### TypeScriptをインストール
    >npm install typescript -g
    >tsc -v

### gulpをインストール
    >npm install gulp -g
    >gulp -v


## これ以降は必要に応じて

### bowerをインストール
フロントエンド開発用のパッケージ管理ツール

    >npm install bower -g
    >bower -v

### tsdをインストール
型定義ファイル管理ツール

    >npm install tsd -g
    >tsd -V

tsdの使い方

    >tsd query jasmine ←jasmineの型定義ファイルを探す
    >tsd query jasmine --action install --save ←jasmineの型定義ファイルをインストールする

### webpackをインストール
モジュールの依存関係を解決するもの

    >npm install webpack -g

### jasmineをインストール
テストツール

    >npm install jasmine -g

### Mocha+shouldをインストール
Mochaはテストツール、shouldはテストコードの文法

    >npm install mocha -g
    >npm install should -g


# 開発環境構築 at SublimeText3

### ArcticTypescript


# プロジェクト開始（testプロジェクト）

### フォルダ作成

    ~/workspace_ts/test2
    ~/workspace_ts/test2/src

### npmを初期化
package.jsonが作られる（手でファイルを作ってもOK）

    >cd ~/workspace_ts/test2
    >npm init

### gulpを初期化
先にpackage.jsonがある場合は`npm install`でpackage.jsonに定義されたパッケージがインストールされる

    >cd ~/workspace_ts/test2
    >npm install --save-dev gulp gulp-typescript

### gulpfile.jsを作成


# 開発

### コーディング
srcフォルダの中にtsファイルを作成する

### コンパイル
各ファイルをコンパイル

    >gulp compile

test7を依存関係を含めてコンパイル

    >gulp compile7

### コマンドラインから実行
コンパイルしたものを実行

    >node built/xxxx.js


# テストコードの文法

### jasmineを初期化

    >cd ~/workspace_ts/test2
    >npm install gulp-jasmine --save-dev
    >jasmine init

### 実行

    >gulp jasmine