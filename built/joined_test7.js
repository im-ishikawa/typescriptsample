var Cat3 = (function () {
    function Cat3() {
    }
    Cat3.prototype.meow = function (s, count) {
        var voice = "にゃーん";
        var result = "";
        var count = (count == null ? 1 : count);
        if (typeof (s) == "string") {
            voice = s;
        }
        for (var i = 0; i < count; i++) {
            result += (i + 1) + voice;
        }
        return result;
    };
    Cat3.prototype.eat = function () {
        this.length += 0.1;
        this.weight += 0.1;
    };
    return Cat3;
}());
var myCat3 = new Cat3();
myCat3.length = 30.5;
myCat3.weight = 2.5;
myCat3.eat();
console.log("6-1:私の猫は" + myCat3.meow() + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");
console.log("6-2:私の猫は" + myCat3.meow("みゃお") + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");
console.log("6-3:私の猫は" + myCat3.meow("みゃお", 3) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");
/// <reference path="test6.ts"/>
var myCat4 = new Cat3();
myCat4.length = 30.5;
myCat4.weight = 2.5;
myCat4.eat();
console.log("7:私の猫は" + myCat4.meow("みゃお") + "と鳴き、\n体長は" + myCat4.length + "cm、体重は" + myCat4.weight + "kgです");
