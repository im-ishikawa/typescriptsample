var gulp = require('gulp');
var typescript = require('gulp-typescript');
var jasmine = require('gulp-jasmine');

var config = {
  ts : {
    src: [
      './src/**/*.ts',       // プロジェクトのルート以下すべてのディレクトリの.tsファイルを対象とする
      '!./node_modules/**' // node_modulesは対象外
    ],
    dst: './built',
    options: { target: 'ES5', module: 'commonjs' }
  }
};

gulp.task('compile', function () {
  return gulp.src(config.ts.src)
             .pipe(typescript(config.ts.options))
             .js
             .pipe(gulp.dest(config.ts.dst));
});

gulp.task('compile7', function() {
  return gulp.src(['./src/test7.ts'])
             .pipe(typescript({ target: "ES5", out: "joined_test7.js" }))
             .pipe(gulp.dest('./built'));
});

gulp.task('jasmine', function() {
  return gulp.src(['./spec/*Spec.ts'])
             .pipe(typescript({ target: "ES5", out: "compiledSpec.js" }))
             .pipe(gulp.dest('./spec'))
             .pipe(jasmine());
});