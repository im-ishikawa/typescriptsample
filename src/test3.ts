//http://www.atmarkit.co.jp/ait/articles/1405/28/news105_3.html
var message: string;

class Cat {
	age: number;
	weight: number;
}

var myCat = new Cat();
myCat.age = 3;
myCat.weight = 5.1;
message = "3:うちのネコは" + myCat.age + "歳で、体重は" + myCat.weight + "kgです";

console.log(message);