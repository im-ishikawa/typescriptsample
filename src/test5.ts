//http://www.atmarkit.co.jp/ait/articles/1507/16/news022_3.html
class Cat2{
	length: number;
	weight: number;
	meow(): string {
		return "にゃーん";
	}
	eat() {
			this.length += 0.1;
			this.weight += 0.1;
	}
}

var myCat2 = new Cat2();
myCat2.length = 30.5;
myCat2.weight = 2.5;

myCat2.eat();

console.log("5:私の猫は" + myCat2.meow() + "と鳴き、\n体長は" + myCat2.length + "cm、体重は" + myCat2.weight + "kgです");