class Cat3 {
		length: number;
		weight: number;
		meow(): string;
		meow(s: string): string;
		meow(s: string, count: number): string;
		meow(s?: any, count?: any): string {
			var voice = "にゃーん";
			var result = "";
			var count = (count == null ? 1 : count);

			if (typeof(s) == "string") {
					voice = s;
			}

			for (var i = 0; i < count; i++) {
				result += (i+1) + voice;
			}

			return result;
		}
		eat() {
				this.length += 0.1;
				this.weight += 0.1;
		}
}

var myCat3 = new Cat3();
myCat3.length = 30.5;
myCat3.weight = 2.5;

myCat3.eat();

console.log("6-1:私の猫は" + myCat3.meow() + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");
console.log("6-2:私の猫は" + myCat3.meow("みゃお") + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");
console.log("6-3:私の猫は" + myCat3.meow("みゃお", 3) + "と鳴き、\n体長は" + myCat3.length + "cm、体重は" + myCat3.weight + "kgです");